const problem6 = require('../problem_6');
const inventory = require('../inventory');

const result = problem6(inventory);
console.log(JSON.stringify(result, null,2));