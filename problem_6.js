function problem6(inventory) {
  if (!Array.isArray(inventory)) {
    return "Input is invalid....Please send an valid Array";
  }
  let audiAndBMW = [];
  for (let i = 0; i < inventory.length; i++) {
    if (inventory[i].car_make === "Audi" || inventory[i].car_make === "BMW") {
      audiAndBMW.push(inventory[i]);
    }
  }
  return audiAndBMW;
}
module.exports = problem6;
