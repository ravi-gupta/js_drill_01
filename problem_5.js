const result = require("./test/testProblem_04");
function problem5(inventory) {
  if (!Array.isArray(inventory)) {
    return "Input is invalid....Please send an valid Array";
  }
  if (!Array.isArray(result)) {
    return "Input from problem 4 is not a valid Array....Please provide a valid array";
  }
  let oldCars = [];
  for (let i = 0; i < inventory.length; i++) {
    if (result[i] < 2000) {
      oldCars.push(inventory[i]);
    }
  }
  console.log(oldCars.length);
  return oldCars;
}
module.exports = problem5;
