function problem3(inventory) {
  if (!Array.isArray(inventory)) {
    return "Input is invalid....Please send an valid Array";
  }
  let carModels = [];
  for (let i = 0; i < inventory.length; i++) {
    let model = inventory[i].car_model;
    if (!carModels.includes(model)) carModels.push(model);
  }
  const output = carModels.sort((a, b) => a.localeCompare(b, undefined, { sensitivity: 'base' }));
  return output;
}
module.exports = problem3;
