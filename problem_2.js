function problem2(inventory) {
  if (!Array.isArray(inventory)) {
    return "Input is invalid....Please send an valid Array";
  }
  if (inventory.length === 0) {
    console.log("Inventory not found");
  }
  const lastCar = inventory[inventory.length - 1];

  const { car_make, car_model } = lastCar;
  const output = `Last car is a ${car_make} ${car_model}`;
  return output;
}
module.exports = problem2;
