function problem4(inventory) {
  if (!Array.isArray(inventory)) {
    return "Input is invalid....Please send an valid Array";
  }
  let carYear = [];
  for (let i = 0; i < inventory.length; i++) {
    carYear.push(inventory[i].car_year);
  }
  return carYear;
}
module.exports = problem4;
